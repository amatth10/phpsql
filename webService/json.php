<?php
	header('Content-type: application/json');
	$imageName = ($_GET['imageName']);
	
	try
	{
		$mysqli_result = DbHandler::executeQuery($filename, $imageName);
		if ($mysqli_result->num_rows > 0) {
			$structure = array();
	        while ($row = mysqli_fetch_assoc($mysqli_result)) {
	            array_push($structure, json_encode($row));
	        }
	        // Check for errors
	        if(json_last_error() == JSON_ERROR_NONE){
	            // No errors occurred, so echo json objects
	            foreach ($structure as $json) {
	                echo $json;
            	}
	        } else{
	            // Errors encountered
	            echo 'Something is wrong with JSON...';
	            echo 'CODE: ' . json_last_error();
	        }
		} else {
	    	exit('Error: No results to display.');
		}
	} catch (Exception $e)
	{
		exit('Error: '.  $e->getMessage());
	}
?>