p1tma/v1.0/README
16/06/15
Luke Matthews
amatth10@mail.bbk.ac.uk

SITE URL

http://titan.dcs.bbk.ac.uk/~amatth10/w1fma/index.php

JSON WEBSERVICE AVAILIBLE AT 

http://titan.dcs.bbk.ac.uk/~amatth10/phpsql/w1fma_old/index.php?page=json&imageName=example.jpg

where example.jpg is the name of the file you are interested in.

CSS copied from  

http://maxdesign.com.au/

The work in this project builds upon my tma as well as material provided in class.

FILE STRUCTURE

├── classes
│   ├── DbHandler.php
│   ├── View.php
│   └── myDB.php
├── css
│   └── styles.css
├── images
│   ├── largeImages
│   └── thumbnails
├── includes
│   ├── config.php
│   ├── helpers.php
│   ├── image_upload.php
│   └── lang
│       ├── en.php
│       └── fr.php
├── index.php
├── readme.txt
├── views
│   ├── 404.php
│   ├── home.php
│   ├── largeImage.php
│   ├── no_content.php // this file is primarily included to allow for quick testing of language settings.
│   ├── templates
│   │   ├── page.html
│   │   └── uploadForm.html
│   └── upload.php
└── webService
    └── json.php





CONFIGURATION 

All files inside w1tma need to be uploaded to server. Keeping the same structure depicted above.
Set the site’s title, database connection, default page template and language in config.php.

DEPLOYMENT

To create a table UNCOMMENT THE FIRST LINE of home.php and then comment it back out - I HAVEN'T TESTED THIS!!!

Adding pages -
1) Add a new file to views e.g. example.php
2) Add a new case to the controller (index.php)
3) Inside your new view write add the line:
   a) $anyVariableName = new View($filename); // it has to be 'View($filename)' not 'View('differentString') if you want the title
                                            // to be set automatically by the HTTP GET request
   b) Set any content you like with:
      $anyVariableName->setSubHead($String)
   
   c) Append info to the content div with:
      $anyVariableName->addContent($String) 

   d) Call $anyVariableName->render();
4) Add a link to it in the page template.html file.
5) OMG you've practically got a new content management system!



