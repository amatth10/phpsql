<?php
/**
 * W1FMA
 * 
 * largeImage.php
 *
 * Luke Matthews 2015
 *
 * This file displays the large image view
 * 
 */
	$largeimagePage = new View($filename);
	$largeimagePage->setSubHead('Large Image');	
	$imageName = ($_GET['imageName']);


	try {
		$mysqli_result = DbHandler::executeQuery($filename, $imageName);
			$content = '<a href="index.php"> <img src="images/largeImages/'.$imageName.'" alt="largeVersionOfImage"/></a>';
	 	if ($mysqli_result->num_rows > 0) {
		// output title and description
		    while($row = $mysqli_result->fetch_assoc()) {
		    	$title = htmlspecialchars($row["title"]);
		    	$description = htmlspecialchars($row["description"]);
		    	$largeimagePage->setSubHead($title);
		        $content.= '<p>'.$description.'</p>';
			}   
		} else {
		   $content.= "<p>Something went wrong!!</p>";
		}

	} catch(Exception $e)
	{
		$content = 'Error: '.  $e->getMessage(). "\n";
	}
	


	$largeimagePage->addContent($content);
	$largeimagePage->render();

?>