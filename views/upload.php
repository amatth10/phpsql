<?php
/**
 * W1FMA
 * 
 * upload.php
 *
 * Luke Matthews 2015
 *
 * This file contains the file upload form, which submits to the same page
 * 
 */
	require SITE_ROOT_DIRECTORY.'/includes/image_upload.php';

	$uploadPage = new View($filename);
	$uploadPage->setSubHead('Upload:');
	$uploadPage->addTemplate('uploadForm');
	$success = '';
	if (isset($_POST['formSubmitted'])) {
		try
		{
			$success = processUploadedFile($_FILES['userfile'], SITE_ROOT_DIRECTORY);
		} catch(Exception $e)
		{	
			$uploadPage->addContent('Error: '.  $e->getMessage(). "\n");
		}
	}
	$uploadPage->addContent($success);
	$uploadPage->render();
?>