<?php
/**
 * W1FMA
 * 
 * home.php
 *
 * Luke Matthews 2015
 *
 * This file displays default landing page for the site and displays the thumbnails
 * UNCOMMENT THE FIRST LINE TO CREATE THE TABLES - I HAVEN'T TESTED THIS!!!
 * UNCOMMENT THE SECOND LINE TO CLEAR THE DATABASE
 */
	//$mysqli_result = DbHandler::executeQuery('createTable'); 
	//$mysqli_result = DbHandler::executeQuery('clearDatabase'); 
	$homePage = new View($filename);
	$homePage->setSubHead('Thumbnails');
	$mysqli_result = DbHandler::executeQuery($filename);
	$content = '';
	if ($mysqli_result->num_rows > 0) {
		// output data of each row
	    while($row = $mysqli_result->fetch_assoc()) {
	    	$imageTitle = htmlspecialchars($row["title"]);
	    	$imageFileName = urlencode(htmlspecialchars($row["filename"]));
	        $content.= '<div class="imageContainer"> <a class="thumbnail_img" href="index.php?page=largeImage&amp;imageName='.urlencode($imageFileName).'">
	        				<img src="images/thumbnails/'.$imageFileName.'" alt="'. $imageTitle .'"/>
							'.$imageTitle .'</a>
						<a class="json" href="index.php?page=json&amp;imageName='.urlencode($imageFileName).'">JSON Data</a> </div>';
	    }
	} else {
	    $content.= "<p>no images uploaded</p>";
	}
	$homePage->addContent($content);
	$homePage->render();
?>