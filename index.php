<?php
	//this is the Controller for the application, it sees which page has been requested 
	//and displays the appropriate view.
	require('./includes/config.php');
	require_once('./includes/helpers.php');
	require ('./includes/lang/'. $config['language'] .'.php');
	// Code to detect whether index.php has been requested without query string
	if(!isset($_GET['page'])){
		$id = 'home';
	} else { 
		$id = $_GET['page'];
	}
	switch ($id) 
	{
		case 'home':
			load($id);
			break;

		case 'upload':
			load($id);
			break;	

		case 'largeImage':
			if(isset($_GET['imageName']))
			{
				load($id);
			} else {
				//the large image file is selected without an appropriate query string so redirect to no content page
				load('no_content');
			}			
			break;

		case 'no_content':
			load($id);
			break;

		case 'json':
			if(isset($_GET['imageName']))
			{
				load($id);
			} else {
				//the large image file is selected without an appropriate query string so redirect to no content page
				echo "the query string was wrong";
				load('no_content');
			}
			break;	

		default :
			load('404');
	}
?>


		

