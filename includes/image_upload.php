<?php
/*
	This file contains three functions:
	processUploadedFile($userFile, $currentDirectory) 

	getMimeType($file)
	imageResize($input_file, $output_file, $req_width, $req_height, $quality)



	Function to process a file upload.
	Takes the a single file uploaded via
	the HTTP POST method, and the path
	to the current directory. 
*/
	function processUploadedFile($userFile, $currentDirectory) {
		if($_POST["image_title"]==""||$_POST["description"]==""){
			throw new Exception("Title and description are required fields.");			
		} else {
			$uploadError = $userFile['error'];
		    if (is_uploaded_file($_FILES['userfile']['tmp_name'])) {
			    if ($uploadError == UPLOAD_ERR_OK) {

			        $fileMimeType = getMimeType($_FILES['userfile']['tmp_name']);
			        if (strpos($fileMimeType, "image/jpeg") !== false) {
			            
			        	$thumbnailDirectory = $currentDirectory.'/images/thumbnails/';
			        	$largeImageDirectory = $currentDirectory.'/images/largeImages/';

			            $uploadedFilename = basename($userFile['name']);

			            $newThumbnailNameWithPath = $thumbnailDirectory.$uploadedFilename;
			            $newLargeImageNameWithPath = $largeImageDirectory.$uploadedFilename;


			            if (file_exists($newThumbnailNameWithPath)||file_exists($newLargeImageNameWithPath)) {
			                throw new Exception("The file already exists! Save the file locally with another name and try uploading it again.");
			            } else {
			                $tmpname = $userFile['tmp_name'];

							try
		                	{
		                		img_resize($tmpname, $newThumbnailNameWithPath, 150, 150, 70);
	                    		$largeImageDimensions = img_resize($tmpname, $newLargeImageNameWithPath, 600, 600, 70);
								try
				                {
				                	$mysqli = DbHandler::insertPostedIntoDatabase($uploadedFilename, $largeImageDimensions);
				                	$mysqli->commit();
				                	return "Image uploaded Sucessfully";
				                } catch (Exception $e) 
				                {
				                	$mysqli->rollback();
								    throw new Exception($e->getMessage(). "\n");
								}
		                	} catch (Exception $e) 
		                	{
		                		throw new Exception($e->getMessage(). "\n");
		                	}
			            }
			        } else {
			            throw new Exception('File type not permitted. Please upload a .jpg file.');
			        }
			    } else if ($error == UPLOAD_ERR_NO_FILE) {
			        throw new Exception("No file selected.");
			    } else if ($error == UPLOAD_ERR_INI_SIZE) {
			        throw new Exception("Maximum file size exceeded.");
			    } else {
			        throw new Exception("Oops. Something went wrong.");
			    }
		    } else {
		    	throw new Exception("Possible file upload attack: filename '". $_FILES['userfile']['tmp_name'] . "'.");
		}
		}   
	}

	function getMimeType($file) {
	    $file_info = new finfo(FILEINFO_MIME);  
	    $mime_type = $file_info->buffer(file_get_contents($file));  
	    return $mime_type;
	}


	function img_resize($input_file, $output_file, $req_width, $req_height, $quality) {

	    // Get image file details
	    list($width, $height, $type, $attr) = getimagesize($input_file);

	    // Open file according to file type
	    if (isset($type)) {
	    	if($type == IMAGETYPE_JPEG){
	    		$img_from_jpeg = @imagecreatefromjpeg($input_file);
	    	} else {
	    		throw new Exception('Image is not a jpeg.');
	    	}
	        
	        // if smaller than requirements
	        if ($width < $req_width and $height < $req_height) {
	            // Use original image dimensions
	            $out_width = $width;
	            $out_height = $height;
	        } else {
	        	//portrait of landscape
	            if ($width > $height) {
	                // landscape
	                $scale = $req_width / $width;
	            } else {
	                // portrait                 
					$scale = $req_height / $height;
	            }
	            $out_width = round($width * $scale);
	            $out_height = round($height * $scale);
	        }
	        $new = imagecreatetruecolor($out_width, $out_height);

	        imagecopyresampled($new, $img_from_jpeg, 0, 0, 0, 0, $out_width, $out_height, $width, $height);

	        imagejpeg($new, $output_file, $quality);

	        imagedestroy($img_from_jpeg);
	        imagedestroy($new);

	        // Return the new width and height
	        return array($out_width, $out_height);
	    } else {
	        throw new Exception("Problem opening image.");
	    }
	}
?>