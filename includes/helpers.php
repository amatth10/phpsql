<?php
	/* 
	a few helper funtions
	*/ 

	function myAutoloader($class)
	{
		// Construct path to the class file
		include dirname(__DIR__) . '/classes/' . $class . '.php';
	}
	spl_autoload_register('myAutoloader');
	
	function load($filename)
	{
		//get path to file
		if($filename == "json")
		{
			$path = __DIR__ . '/../webService/' . $filename . '.php';
		} else{
			$path = __DIR__ . '/../views/' . $filename . '.php';
		}


		if (file_exists($path))
		{		
			require($path);
		}

	}
?>
