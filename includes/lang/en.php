<?php
	define('DEFAULT_HEADING', 'My Site');
	define('DEFAULT_SUBHEAD', 'This is the default sub-heading.');
	define('DEFAULT_CONTENT', 'This page has no content.');
	define('DATABASE_ERROR', 'Sorry, there was a problem connecting to the database: ');
?>