<?php
	class DbHandler{
		// connects to the database using the credentials saved in config.php
		
		static function executeQuery($queryName, $filename = false)
		{		
			$DBobj = DbHandler::connectToDb();	
			//get the appropriate query for the page selected
			$query = DbHandler::selectQuery($queryName, $filename, $DBobj);
			// make the query	
			$result = $DBobj->query($query);
			//check completion
			if ($result === false){
				throw new Exception("Database error: " . $DBobj->error); //failure point
			} else {
				return $result;	
			}
		}
		static function insertPostedIntoDatabase($newname, $largeImageDimensions) {

			$DBobj = DbHandler::connectToDb();	
			$DBobj->autocommit(false);
			list($imageWidth, $imageHeight) = $largeImageDimensions;
			$escapedWidth = $DBobj->real_escape_string($imageWidth);
			$escapedHeight = $DBobj->real_escape_string($imageHeight);
			$escapedTitle = $DBobj->real_escape_string($_POST["image_title"]);
			$escapedDescription = $DBobj->real_escape_string($_POST["description"]);
			$escapedFileName = $DBobj->real_escape_string($newname);

			$sql = "INSERT INTO images (title, description, filename, width, height)
 			VALUES('".$escapedTitle."','".$escapedDescription."','".$escapedFileName."','".$escapedWidth."','".$escapedHeight."');
 			";

			$result = $DBobj->query($sql);
			if ($result === false){///TRANSACTION FAILURE POINT
				throw new Exception("Database error: " . $DBobj->error);
			} else {
				return $DBobj;	
			}
		}

		static function connectToDb(){
			require('./includes/config.php');
			 // connect to db
			 $db = new myDB($config['data_server'], 
							$config['data_user'], 
							$config['data_pass'], 
							$config['data_schema']);
			return $db;
		}

		// picks the appropriate query for the page selected
		static function selectQuery($queryName, $filename = false, $DBobj)
		{
			//if query is to include a string that the user could add to the url escape it!
			if($filename){
				$filename = $DBobj->escape_string($filename);
			}
			switch ($queryName) 
			{	
				case 'createTable':
					$sql = "CREATE TABLE images (id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY, title VARCHAR(63) NOT NULL, description VARCHAR(255) NOT NULL, filename VARCHAR(63) NOT NULL, width INT(3) NOT NULL, height INT(3)";
				case 'clearDatabase':
					$sql = "TRUNCATE amatth10db.images";
					break;
				case 'home':
					$sql = "SELECT title, filename FROM images";
					break;
				case 'json':
					$sql = "SELECT title, description, filename, width, height FROM images WHERE filename ='$filename'";
					break;
				case 'largeImage':
					$sql = "SELECT title, description  FROM images WHERE filename ='$filename'";
					break;
				default :
					echo "selectQuery() was called with an invalid parameter: ".$queryName;
			}
			
			return $sql;
		}

		
	}
?>
