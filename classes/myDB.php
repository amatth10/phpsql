<?php
	class myDB extends mysqli {
		public function __construct($host,$user,$pass,$db){
			// Call the parent constructor
			parent::__construct($host, $user, $pass, $db);
			// Use $this to access mysqli members
			if($this->connect_errno){
				echo DATABASE_ERROR;
				exit($this->connect_error);
			}
		}
	}
	
?>