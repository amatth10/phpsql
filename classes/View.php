<?php
	/*
	public helpers

	setSubHead(String) sets the sub heading for the page
	addContent(String) adds content to the page	
	addTalbeToContent(String, array()) takes the name of the query from the availible cases in DbHandler or a two array of strings
									   and return the appropriate table
	render() executes the page's rendering

	*/

	class View 
	{
	    private $pageTitle;
	    private $heading;
	    private $subHead;
	    private $content;
	    private $placeholders = array();
	    private $templateFile;

		public function __construct($pageTitle) 
		{ 
			$this->heading = SITE_TITLE;
			$this->pageTitle = $pageTitle;
			$this->templateFile = DEFAULT_PAGE_TEMPLATE;
			//placeHolders 
			//DO NOT CHANGE THE ORDER OF THESE! 
			//IT WILL MEAN THE DATA WILL END UP IN THE WRONG PLACE
			$this->placeholders['page_title'] = '[+title+]';
			$this->placeholders['site_title'] = '[+heading+]';
			$this->placeholders['subHead'] = '[+subHeading+]';
			$this->placeholders['content'] = '[+content+]';
		}

		public function addContent($string)
		{
			$this->content .= $string;
		}
		public function addTemplate($name)
		{
			$this->content = file_get_contents(SITE_ROOT_DIRECTORY.'/views/templates/'.$name.'.html');
		}

		public function setSubHead($string)
		{
			$this->subHead = $string;
		}
		public function render()
		{
			$this->setContentIfNull();
			$pageData = $this->pageElementsToArray();
			$output = $this->replaceHolders($pageData);	
			echo $output;
		}
		private function setContentIfNull()
		{
			if(!isset($this->heading))
			{
				$this->heading = DEFAULT_HEADING;
			}
			if(!isset($this->subHead))
			{
				$this->subHead = DEFAULT_SUBHEAD;
			}
			if(!isset($this->content))
			{
				$this->content = DEFAULT_CONTENT;
			}
		}
		private function pageElementsToArray()
		{
			$result = array();
			$result[] = $this->pageTitle;
			$result[] = $this->heading;
			$result[] = $this->subHead;
			$result[] = $this->content;
			return $result;
		}

		//replaces the main view placeholders with relevant data
		private function rePlaceHolders($pageData)
		{
			$tpl = file_get_contents($this->templateFile);
			$output = str_replace($this->placeholders, $pageData, $tpl);
			return $output;			
		}		
	}
?>